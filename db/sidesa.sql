-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jun 25, 2018 at 04:05 PM
-- Server version: 10.1.30-MariaDB-0ubuntu0.17.10.1
-- PHP Version: 7.1.17-0ubuntu0.17.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sidesa2`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_penduduk`
--

CREATE TABLE `tb_penduduk` (
  `id_penduduk` int(20) NOT NULL,
  `NKK` varchar(50) NOT NULL,
  `NIK` varchar(50) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(50) NOT NULL,
  `tempat_lahir` varchar(50) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `golongan_darah` varchar(50) NOT NULL,
  `agama` varchar(20) NOT NULL,
  `status` varchar(50) NOT NULL,
  `hubungan_keluarga` varchar(50) NOT NULL,
  `pendidikan` varchar(50) NOT NULL,
  `pekerjaan` varchar(50) NOT NULL,
  `nama_ibu` varchar(50) NOT NULL,
  `nama_ayah` varchar(50) NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `rt` varchar(50) NOT NULL,
  `rw` varchar(50) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_penduduk`
--

INSERT INTO `tb_penduduk` (`id_penduduk`, `NKK`, `NIK`, `nama`, `jenis_kelamin`, `tempat_lahir`, `tanggal_lahir`, `golongan_darah`, `agama`, `status`, `hubungan_keluarga`, `pendidikan`, `pekerjaan`, `nama_ibu`, `nama_ayah`, `alamat`, `rt`, `rw`, `created_at`, `updated_at`) VALUES
(11, '1234567890123456', '0987654321098765', 'Rudi Tawangga', 'Laki-laki', 'Indramayu', '1997-06-02', 'O', 'Islam', 'Belum Kawin', 'Anak', 'Akademi/Diploma III/S. Muda', 'Pelajar/Mahasiswa', 'Warnisem', 'Runjaya', 'Ds. Sleman lor Blok Tlaga', '04', '02', '2018-05-12 23:16:25', '2018-05-17 21:18:05');

-- --------------------------------------------------------

--
-- Table structure for table `tb_profile_desa`
--

CREATE TABLE `tb_profile_desa` (
  `id` int(11) NOT NULL,
  `nama_kecamatan` varchar(50) NOT NULL,
  `nama_desa` varchar(50) NOT NULL,
  `alamat_balai_desa` varchar(255) NOT NULL,
  `nama_kuwu` varchar(50) NOT NULL,
  `kode_pos` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tb_report`
--

CREATE TABLE `tb_report` (
  `id_report` int(11) NOT NULL,
  `id_katergori_surat` int(11) NOT NULL,
  `id_jenis_surat` int(11) NOT NULL,
  `id_surat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_report`
--

INSERT INTO `tb_report` (`id_report`, `id_katergori_surat`, `id_jenis_surat`, `id_surat`) VALUES
(6, 1, 2, 8);

-- --------------------------------------------------------

--
-- Table structure for table `tb_user`
--

CREATE TABLE `tb_user` (
  `id_user` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nama_petugas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_user`
--

INSERT INTO `tb_user` (`id_user`, `username`, `password`, `nama_petugas`) VALUES
(1, 'admin', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `tp_kuasa_aktifasinomortelp`
--

CREATE TABLE `tp_kuasa_aktifasinomortelp` (
  `id_surat_aktfno` int(11) NOT NULL,
  `id_jenis_surat` int(11) NOT NULL,
  `p1_nama` varchar(50) NOT NULL,
  `p1_tempat_lahir` varchar(50) NOT NULL,
  `p1_tgl_lahir` date NOT NULL,
  `p1_pekerjaan` varchar(50) NOT NULL,
  `p1_alamat` text NOT NULL,
  `p2_nama` varchar(50) NOT NULL,
  `p2_tempat_lahir` varchar(50) NOT NULL,
  `p2_tgl_lahir` date NOT NULL,
  `p2_pekerjaan` varchar(50) NOT NULL,
  `p2_alamat` text NOT NULL,
  `hari` varchar(50) NOT NULL,
  `tanggal` varchar(50) NOT NULL,
  `bulan` varchar(50) NOT NULL,
  `tahun` varchar(50) NOT NULL,
  `tgl_lengkap` date NOT NULL,
  `nama_anak` varchar(50) NOT NULL,
  `Operator` varchar(50) NOT NULL,
  `nomor_telp` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tp_kuasa_gonogini`
--

CREATE TABLE `tp_kuasa_gonogini` (
  `id_surat_gg` int(11) NOT NULL,
  `id_jenis_surat` int(11) NOT NULL,
  `p1_nama` varchar(50) NOT NULL,
  `p1_tempat_lahir` varchar(50) NOT NULL,
  `p1_tgl_lahir` date NOT NULL,
  `p1_pekerjaan` varchar(50) NOT NULL,
  `p1_alamat` text NOT NULL,
  `p2_nama` varchar(50) NOT NULL,
  `p2_tempat_lahir` varchar(50) NOT NULL,
  `p2_tgl_lahir` date NOT NULL,
  `p2_pekerjaan` varchar(50) NOT NULL,
  `p2_alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tp_kuasa_hakasuh`
--

CREATE TABLE `tp_kuasa_hakasuh` (
  `id_surat_ha` int(11) NOT NULL,
  `id_jenis_surat` int(11) NOT NULL,
  `p1_nama` varchar(50) NOT NULL,
  `p1_tempat_lahir` varchar(50) NOT NULL,
  `p1_tgl_lahir` date NOT NULL,
  `p1_pekerjaan` varchar(50) NOT NULL,
  `p1_alamat` text NOT NULL,
  `p2_nama` varchar(50) NOT NULL,
  `p2_tempat_lahir` varchar(50) NOT NULL,
  `p2_tgl_lahir` date NOT NULL,
  `p2_pekerjaan` varchar(50) NOT NULL,
  `p2_alamat` text NOT NULL,
  `hari` varchar(50) NOT NULL,
  `tanggal` varchar(50) NOT NULL,
  `bulan` varchar(50) NOT NULL,
  `tahun` varchar(50) NOT NULL,
  `tgl_lengkap` date NOT NULL,
  `nama_anak` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tp_kuasa_kk`
--

CREATE TABLE `tp_kuasa_kk` (
  `id_surat_kk` int(11) NOT NULL,
  `p1_nama` varchar(50) NOT NULL,
  `p1_tempat_lahir` varchar(50) NOT NULL,
  `p1_tgl_lahir` date NOT NULL,
  `p1_pekerjaan` varchar(50) NOT NULL,
  `p1_alamat` text NOT NULL,
  `p2_nama` varchar(50) NOT NULL,
  `p2_tempat_lahir` varchar(50) NOT NULL,
  `p2_tgl_lahir` date NOT NULL,
  `p2_pekerjaan` varchar(50) NOT NULL,
  `p2_alamat` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tp_kuasa_kk`
--

INSERT INTO `tp_kuasa_kk` (`id_surat_kk`, `p1_nama`, `p1_tempat_lahir`, `p1_tgl_lahir`, `p1_pekerjaan`, `p1_alamat`, `p2_nama`, `p2_tempat_lahir`, `p2_tgl_lahir`, `p2_pekerjaan`, `p2_alamat`) VALUES
(8, 'test', 'test', '2018-05-04', 'test', 'tset', 'oke', 'oke', '2018-05-04', 'oke', 'oke');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_penduduk`
--
ALTER TABLE `tb_penduduk`
  ADD PRIMARY KEY (`id_penduduk`),
  ADD UNIQUE KEY `NIK` (`NIK`);

--
-- Indexes for table `tb_profile_desa`
--
ALTER TABLE `tb_profile_desa`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_report`
--
ALTER TABLE `tb_report`
  ADD PRIMARY KEY (`id_report`),
  ADD KEY `id_katergori_surat` (`id_katergori_surat`),
  ADD KEY `id_surat` (`id_surat`),
  ADD KEY `id_jenis_surat` (`id_jenis_surat`);

--
-- Indexes for table `tb_user`
--
ALTER TABLE `tb_user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indexes for table `tp_kuasa_aktifasinomortelp`
--
ALTER TABLE `tp_kuasa_aktifasinomortelp`
  ADD PRIMARY KEY (`id_surat_aktfno`),
  ADD KEY `id_surat_aktfno` (`id_surat_aktfno`),
  ADD KEY `id_jenis_surat` (`id_jenis_surat`);

--
-- Indexes for table `tp_kuasa_gonogini`
--
ALTER TABLE `tp_kuasa_gonogini`
  ADD PRIMARY KEY (`id_surat_gg`),
  ADD KEY `id_surat_gg` (`id_surat_gg`),
  ADD KEY `id_kategori_surat` (`id_jenis_surat`);

--
-- Indexes for table `tp_kuasa_hakasuh`
--
ALTER TABLE `tp_kuasa_hakasuh`
  ADD PRIMARY KEY (`id_surat_ha`),
  ADD KEY `id_kategori_surat` (`id_jenis_surat`),
  ADD KEY `id_surat_ha` (`id_surat_ha`);

--
-- Indexes for table `tp_kuasa_kk`
--
ALTER TABLE `tp_kuasa_kk`
  ADD PRIMARY KEY (`id_surat_kk`),
  ADD KEY `id_surat_kk` (`id_surat_kk`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_penduduk`
--
ALTER TABLE `tb_penduduk`
  MODIFY `id_penduduk` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tb_profile_desa`
--
ALTER TABLE `tb_profile_desa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tb_report`
--
ALTER TABLE `tb_report`
  MODIFY `id_report` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `tb_user`
--
ALTER TABLE `tb_user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `tp_kuasa_hakasuh`
--
ALTER TABLE `tp_kuasa_hakasuh`
  MODIFY `id_surat_ha` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tp_kuasa_kk`
--
ALTER TABLE `tp_kuasa_kk`
  MODIFY `id_surat_kk` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
