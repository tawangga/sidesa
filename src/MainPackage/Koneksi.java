/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MainPackage;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


/**
 *
 * @author tawangga
 */
public class Koneksi {
    public static Connection con;
    public static Statement stm;
    private String url = "jdbc:mysql://localhost/sidesa";
    private String user = "root";
    private String password = "";
    public Koneksi(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection(url,user,password);
            stm = con.createStatement();
            System.out.println("Koneksi Berhasil");
        } catch (Exception ex) {
//            System.err.println("Gagal Koneksi karena "+ex.getMessage());
                JOptionPane.showMessageDialog(null, "Gagal Koneksi Karna "+ex.getMessage());
        }
    }    
    public void closeConnection() throws SQLException{
        con.close();
    }
}
